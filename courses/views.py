from django.shortcuts import render
from rest_framework import viewsets
from .models import Course, Instructor
from .serializers import CourseSerializer, InstructorSerializer

class CourseView(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

class InstructorView(viewsets.ModelViewSet):
    queryset = Instructor.objects.all()
    serializer_class = InstructorSerializer
