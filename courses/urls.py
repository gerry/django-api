from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('courses', views.CourseView)
router.register('instructors', views.InstructorView)

urlpatterns = [
    path('', include(router.urls))
]
